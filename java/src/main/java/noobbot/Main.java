package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import java.util.ArrayList;
import static noobbot.Estat.getAndRemoveFromLTM;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                LinkedTreeMap ltm = null;
                msgFromServer.data.getClass().cast(ltm);
                ArrayList a = (ArrayList) msgFromServer.data;
                for(int i = 0; i < a.size(); i++){
                    System.out.println(a.get(i));
                }
                send(new Throttle(defineNewThrottle(ltm)));                
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("lapFinished")) {
                manageLapFinished((LinkedTreeMap) msgFromServer.data);
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
    
    private double defineNewThrottle(LinkedTreeMap ltm){
        Estat estat = Estat.decodeEstat(ltm);
        
        //if(estat.piece.)
        return 0.5;
    }

    private void manageLapFinished(LinkedTreeMap ltm) {
        System.out.println("Lap DONE!");
        System.out.println("lap = "+ltm.get("lap"));
        System.out.println("millis = "+ltm.get("millis"));        
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Estat {
    public String name, color;
    public double angle;
    public PiecePosition piece;
    public int lap;

    public Estat(){}
    
    public Estat(String name, String color, double angle, PiecePosition piece, int lap) {
        this.name = name;
        this.color = color;
        this.angle = angle;
        this.piece = piece;
        this.lap = lap;
    }
    
    public static Estat decodeEstat(LinkedTreeMap ltm){
        Estat estat = new Estat();
        estat.name = (String) getAndRemoveFromLTM(ltm, "name");
        estat.color = (String) getAndRemoveFromLTM(ltm, "color");
        estat.angle = (double) getAndRemoveFromLTM(ltm, "angle");
        estat.piece = PiecePosition.decodePiece(ltm);
        return estat;
    }
    
    public static Object getAndRemoveFromLTM(LinkedTreeMap ltm, String key){
        if(ltm.containsKey(key)) return ltm.remove(key);
        return null;
    }    
}

class PiecePosition {
    public int pieceIndex;
    public double inPieceDistance;
    public int startLaneIndex, endLaneIndex;

    public PiecePosition() {}
    
    public PiecePosition(int pieceIndex, double inPieceDistance, int startLaneIndex, int endLaneIndex) {
        this.pieceIndex = pieceIndex;
        this.inPieceDistance = inPieceDistance;
        this.startLaneIndex = startLaneIndex;
        this.endLaneIndex = endLaneIndex;
    }
    
    public static PiecePosition decodePiece(LinkedTreeMap ltm){
        PiecePosition pp = new PiecePosition();
        pp.pieceIndex = (int) getAndRemoveFromLTM(ltm, "pieceIndex");
        pp.inPieceDistance = (double) getAndRemoveFromLTM(ltm, "inPieceDistance");
        pp.startLaneIndex = (int) getAndRemoveFromLTM(ltm, "startLaneIndex");
        pp.endLaneIndex = (int) getAndRemoveFromLTM(ltm, "endLaneIndex");
        return pp;
    }
    
    public static Object getAndRemoveFromLTM(LinkedTreeMap ltm, String key){
        if(ltm.containsKey(key)) return ltm.remove(key);
        return null;
    }
}